% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/qa.r
\name{run_qa}
\alias{run_qa}
\title{Apply Question-Answering on a string}
\usage{
run_qa(
  text,
  questions,
  model = "trftc_biography:en:qa",
  max_answers = 1,
  threshold = NULL,
  verbose = FALSE
)
}
\arguments{
\item{text}{to be analyzed}

\item{questions}{a vector of questions or a named list of question vectors
(see biography_questions() for an example)}

\item{model}{to be used (use `list_qa_models()` to get available models)}

\item{max_answers}{an integer or a named list with keys corresponding to keys
in `questions`. If not a named list, value will be used for all questions.}

\item{threshold}{a float, NULL or a named list with keys corresponding to
keys in `questions`. If not a named list, value will be used for all
questions.}
}
\description{
Apply Question-Answering on a string
}
