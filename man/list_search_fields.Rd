% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/search.r
\name{list_search_fields}
\alias{list_search_fields}
\title{List possible search fields for a given corpus}
\usage{
list_search_fields(corpus, verbose = FALSE)
}
\arguments{
\item{corpus}{the name of the corpus}
}
\value{
a vector of strings, listing the search fields
}
\description{
These fields are useful for advanced search functions
such as search_concordance_ex
}
