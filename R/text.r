library(stringr)

sylRE = "(?i)(p|p'|ts|ts'|tz|tz'|ch'|ch'|ch|ch|t|t'|f|k'|k|h|l|m|n|j|s|ss|sh|w|y|hs|)(ih|eh|erh?|[aeiouwü]+(?:ng|n|h)?)"

#' wade-giles to pinyin conversion
#'
#'
#' @param wade a string in wade-giles romanisation
#' @return a string in pinyin romanisation
#' @import stringr
#' @export
wade_to_py <- Vectorize(function(wade) {
  if(is.na(wade)) wade
  else {
    str_replace_all(wade,sylRE,function(syl) {
      m = str_match(syl,sylRE)
      i = m[2]
      f = m[3]
      str_c(initialMapping(i,f),finalMapping(i,f))
    })
  }
})

finalMapping <- function(i,f) {
  py = str_to_lower(f)
  if(str_ends(py,"h")) {
    if(f == "eh" && i != "y") py = "ei"
    else py = str_sub(py,0,-2)
  }
  py = str_replace(py, "ê","e")
  if(py=="o") {
    if(str_detect(i,"^(k'|k|h)$")) py = "e"
    else if(str_detect(i,"^[wbmpf]$")) py = "o"
    else py = "uo"
  }
  py = str_replace(py, "uei","ui")
  py = str_replace(py,"ien","ian")
  py = str_replace(py,"uen","uan")
  py = str_replace(py,"ung","ong")

  if(i == "y") {
    if(py == "u") py = "ou"
    py = str_replace(py, "en", "an")
  }

  if(str_detect(i,"^(y|ch|ch'|hs)$")) py = str_replace(py, "ü","u")
  if(str_detect(i,"^(ss|tz)$") && py == "u") py = "i"
  py = str_replace(py,"w","u")
  py
}


initialMapping <- function(i,f) {
  if(i == ""){
    if(f == "i") "y"
    else if(f =="I") "Y"
    else ""
  }
  else {
    py = switch (str_to_lower(i),
            "p" = "b",
            "p'" = "p",
            "ts" = "z",
            "ts'" = "c",
            "tz" = "z",
            "tz'" = "c",
            "ch'" = if(str_starts(f,"i") || str_starts(f,"ü")) "q" else "ch",
            "ch"  = if((str_starts(f,"i") && f != "ih") || str_starts(f,"ü")) "j" else "zh",
            "t" = "d",
            "t'" = "t",
            "f" = "f",
            "k'" = if(str_starts(f,"i") || str_starts(f,"ü")) "q" else "k",
            "k"  = if(str_starts(f,"i") || str_starts(f,"ü")) "j" else "g",
            "h" = "h",
            "l" = "l",
            "m" = "m",
            "n" = "n",
            "j" = "r",
            "s" = if(f == "i") "x" else "s",
            "ss" = "s",
            "sh" = "sh",
            "w" = "w",
            "y" = "y",
            "hs" = "x"
    )
    if(str_detect(i,"^[A-Z]")) str_to_title(py)
    else py
  }
}



# run this to create de R/sysdata.rda file with pinyin mapping
create_sino_py_mapping <- function() {
  library(readr)
  library(dplyr)
  mandarin <- read_delim("data-raw/mandarin.csv",
                         "\t", escape_double = FALSE, col_names = FALSE,
                         col_types = cols(X1 = col_character()),
                         comment = "#", trim_ws = TRUE)
  conv <- Vectorize(function(x){ intToUtf8(strtoi(stringr::str_replace(x,"U\\+","0x")))}, USE.NAMES = FALSE)
  sino_to_py <- mandarin %>%
    transmute(
      Sino=conv(X1),
      PY=stringr::str_replace_all(stringr::str_replace(X3,"^.*:",""),",","/")
    )
  usethis::use_data(sino_to_py, internal = TRUE, overwrite = TRUE)
}


#' sinograms(漢字) to pinyin conversion
#'
#'
#' @param hanzi a string of sinograms
#' @return a string in pinyin romanisation
#' @import stringr
#' @export
sinograms_to_py <- Vectorize(function(hanzi) {
  str_c((tibble::as.tibble(cbind(Sino=strsplit(hanzi,NULL)[[1]])) %>% dplyr::inner_join(sino_to_py, by="Sino"))$PY, collapse=" ")
})

#' apply a collection of Regexps to a collection of documents
#'
#'
#' @param corpus a table with the documents (must include DocId, Title and Text columns)
#' @param regexps a table with pattern to look for (with a Regexp and a Type columns)
#' @return a table with all the matches
#' @import stringr
#' @import dplyr
#' @import tidyr
#' @export
extract_regexps_from_subcorpus <- function(corpus, regexps) {
  result = NULL
  batch_size = 1000
  regexps <- regexps %>% mutate(fake=1)
  nDocs = corpus %>% nrow()
  for(i in 0:ceiling(nDocs/batch_size)) {
    subcorpus <- corpus %>% slice((i*batch_size):((i+1)*batch_size))
    new_result = subcorpus %>% mutate(fake=1) %>%
      full_join(regexps, by="fake") %>%
      transmute(
        DocId=DocId,
        Type=Type,
        Match=coalesce(str_extract(Title,Regexp), str_extract(Text, Regexp))
      ) %>% na.omit()
    if(is.null(result)) {result <- new_result}
    else {result <- bind_rows(result, new_result)}
  }
  result
}


match_concordance <- function(text, id, query, context_size,
                              space_is_word_sep = FALSE,
                              use_regexp = FALSE,
                              case_sensitive = FALSE,
                              id_column = "rowid") {
  if (!use_regexp) {
    query <- gsub("(\\\\^|\\$|\\.|\\||\\?|\\*|\\+|\\(|\\)|\\[|\\{)",
                  "\\\\\\1", query)
  }
  if (space_is_word_sep) {
    query <- paste0("(?<=\\s|^)", query, "(?=\\s|$|[[:punct:]])")
  }
  window_size <- context_size %/% 2
  subtext <- text
  offset <- 0
  before <- c()
  match_str <- c()
  after <- c()
  while (length(subtext) != 0) {
    matched_query <- regexpr(query, subtext, ignore.case = !case_sensitive,
                             perl = TRUE)
    if (matched_query == -1) {
      break
    }
    start <- matched_query + offset
    end <- start + attr(matched_query, "match.length")

    before <- c(before, substr(text, start - window_size, start - 1))
    match_str <- c(match_str, substr(text, start, end - 1))
    after <- c(after, substr(text, end, end + window_size - 1))

    subtext <- substring(subtext, matched_query + attr(matched_query,
                                                       "match.length"))
    offset <- end - 1
  }

  tibble::tibble(!!(id_column) := rep(id, length(match_str)),
                 Before = before, Match = match_str, After = after)
}

#' KWIC search in a custom dataframe
#'
#' @param df a dataframe which contains the text to search in.
#' @param query a simple text query or a regex pattern (if the latter,
#'   use_regexp must be set to TRUE).
#' @param context_size length of the text snippets to be returned around the
#'   matches.
#' @param search_column name of the column to use in the dataframe.
#' @param id_column name of the column which contains unique row ids. If NULL, a
#'   new 'rowid' column will be created.
#' @param space_is_word_sep if TRUE, will consider spaces as word delimiters.
#' @param use_regexp if TRUE, will consider `query` as a regexp pattern (using
#'   PERL syntax).
#' @param case_sensitive if TRUE, query and matches must also have matching
#'   letter cases.
#' @return a tibble dataframe with matches and surrounding contexts.
#' @export
search_concordance_on_df <- function(df, query, context_size = 30,
                                     search_column = "Text",
                                     id_column = NULL,
                                     space_is_word_sep = FALSE,
                                     use_regexp = FALSE,
                                     case_sensitive = FALSE) {
  if (is.null(id_column)) {
    id_column <- "rowid"
    df <- tibble::rowid_to_column(df)
  }
  purrr::map_dfr(seq_along(df[[search_column]]), function(i) {
    match_concordance(df[[search_column]][[i]], df[[id_column]][[i]],
                      query, context_size,
                      space_is_word_sep = space_is_word_sep,
                      use_regexp = use_regexp,
                      case_sensitive = case_sensitive,
                      id_column = id_column)
  })
  # dplyr::right_join(df, conc_df, by = id_column)
}
