
#' Load the text from a PDF into a data frame
#'
#' @param filepath string that contains the path to the PDF file
#' @param min_text_length minimum number of characters that a page must contain
#'   in order to be stored in the data.frame (default: 1)
#' @param identifier optional string to use as a basename for identifiers in a
#'   'id' column. No 'id' column is added if NULL is given (default: "PDF")
#' @param merge_lines_char character used to merge text lines in a page (default: " ")
#' @return a data.frame with at least the two columns 'page' and 'text'
#' @note Requires the 'pdftools' package
#' @export
load_pdf_as_df <- function(filepath, min_text_length=1, identifier="PDF",
                           merge_lines_char = " ") {
  if (!requireNamespace("pdftools", quietly = TRUE)) {
    stop("Package \"pdftools\" needed for this function to work. Please install it.",
      call. = FALSE
    )
  }

  ## pdf_data returns more information so it will be easier to add things with it
  text_list <- tryCatch({
    pdf_data <- pdftools::pdf_data(filepath)
    unlist(lapply(1:length(pdf_data), function(x) {
      paste(pdf_data[[x]]$text, collapse = merge_line_char)
    }))
  }, error = function(e) {
    pdf_text <- trimws(pdftools::pdf_text(filepath))
    gsub("\n", merge_lines_char, gsub("[ \t]+", " ", pdf_text))
  })
  df <- data.frame(Page = 1:length(text_list), Text = text_list)
  df <- df[nchar(df$Text) >= min_text_length, ]

  if (dim(df)[1] == 0) {
    warning("PDF is empty: make sure it contains text (may need to OCRize first!) or lower the value of min_text_length")
    return(NULL)
  }

  if (!is.null(identifier)) {
    df$Id <- paste(identifier, df$Page, sep = "-")
  }

  dplyr::as_tibble(df)
}
