library(ggplot2)
library(gridExtra)
library(stringr)
library(dplyr)
library(plotly)
library(tidytext)
library(purrr)
library(tidyr)
library(tidyverse)

#' Apply stats count on the specified column of a dataframe 
#'
#' @param df Dataframe which contains texts to analyze
#' @param query_text keyword(s) to look in the text.
#' @param to_plot if True  returns a plot else a dataframe.
#' @param over_time Count the frequency over time
#' @param by_char Count the frequency over time compare to the complete count of character by time
#' @param ly return a plotly  graph of a classic ggplot.
#' @note "q = AND | OR"
#' @export
stats_count <- function(df,
                      query_text,
                      to_plot = TRUE,
                      over_time = FALSE,
                      by_char = FALSE,
                      ly = TRUE
                    ) {

    if (!requireNamespace("ggplot2", quietly = TRUE)) {
        stop("Package \"ggplot2\" needed for this function to work. Please install it.",
        call. = FALSE
        )
    }
    if (!requireNamespace("plotly", quietly = TRUE)) {
        stop("Package \"plotly\" needed for this function to work. Please install it.",
        call. = FALSE
        )
    }
    if (!requireNamespace("purrr", quietly = TRUE)) {
        stop("Package \"ggplot2\" needed for this function to work. Please install it.",
        call. = FALSE
        )
    }
    library(ggplot2)
    library(plotly)
    df <- data.frame(df) 
    

    to_look <- gsub('"','' ,query_text)
    df$word_count <- str_count(df$Text,regex(to_look, ignore_case = TRUE))

    search_terms <- str_split(to_look, " AND | OR ", simplify = TRUE)
    # iterate over each search term and count its occurrences
    sr_counts <- purrr::map(search_terms, ~ {
        df$word_count <- str_count(df$Text, regex(.x, ignore_case = TRUE))
        sr_counts <- df %>%
        group_by(word_count) %>%
        summarise(n_articles = n()) %>%
        mutate(search_term = .x)
        return(sr_counts)
    })
    word_counts <- bind_rows(sr_counts)


    if (over_time){
        
        df$Date <- as.numeric(as.character(df$Date))

        if (by_char){


            sr_counts <- lapply(search_terms, function(term) {
                df$word_count <- str_count(df$Text, regex(term, ignore_case = TRUE))
                sr_counts <- df %>%
                group_by(Date) %>%
                summarise(word_count = sum(word_count)) %>%
                mutate(search_term = term)
                return(sr_counts)
            })
            # Calculate complete word count for each Date
            complete_counts <- df %>%
                group_by(Date) %>%
                summarise(complete_count = sum(str_count(Text, "\\S+")))
            # Combine search term counts and complete counts
            combined_counts <- bind_rows(sr_counts) %>%
                left_join(complete_counts, by = "Date")

            if (to_plot){
                # Plotting
                stats_count <- ggplot(combined_counts, 
                    aes(x = Date, y = word_count / complete_count,
                        color = search_term)) +
                    geom_line() +
                    xlab("Date") +
                    ylab("Word Count Percentage") +
                    scale_color_discrete(name = "Search Term")

                plot <- ggplotly(stats_count) %>%
                    layout(annotations = list(
                    list(
                        x = 0.5,
                        y = 1.1,
                        text = "Word Count Percentage of Search Terms Compared to Complete Word Count",
                        showarrow = FALSE,
                        xref = 'paper',
                        yref = 'paper'
                    )
                    ))

                attr(plot,"text") <-  "Word Count Percentage of Search Terms Compared to Complete Word Count"
                if (ly){
                    plot
                } else {
                    stats_count
                }
                
            
            } else {
                combined_counts
            }
        } else {
            to_look_words <- strsplit(to_look, " AND | OR ")[[1]]

            # Compute word count for each word separately
            sr_word <- data.frame()

            # Loop through each word in to_look and calculate word count and mean word count
            for (word in to_look_words) {
                # Calculate word count for each row in sr
                sr <- df %>%
                mutate(word_count = str_count(Text, regex(word, ignore_case = TRUE)))

                # Calculate mean word count for each year in sr
                sr_mean <- df %>%
                group_by(Date) %>%
                summarize(mean_word_count = mean(word_count))

                # Add word as column to sr_mean
                sr_mean$word <- word

                # Append results to sr_word data frame
                sr_word <- rbind(sr_word, sr_mean)
            }
            if (to_plot){
            # Plot bar chart
                stats_count_year <- ggplot(sr_word, aes(x = Date, y = mean_word_count, fill = word)) +
                    geom_bar(stat = "identity", position = "dodge") +
                    labs(x = "Year", y = "Mean Word Count") +
                    scale_fill_discrete(drop=TRUE)
                plot <- ggplotly(stats_count_year)  %>% layout(annotations = list(list(x = 0.5 , y = 1.1, text = paste0("Occurrences of '", to_look, "' in Text Fields by Year"),
                                                                            showarrow = F, xref='paper', yref='paper')))
                attr(plot,"text") <- paste0("Occurrences of '", to_look, "' in Text Fields by Year")
                if (ly){
                    plot
                } else {
                    stats_count_year
                }
            } else {
                sr_word
            }
        }
    } else {
        if (to_plot) {
            stats_count <- ggplot(word_counts, aes(x = factor(word_count),
                y = n_articles, fill = search_term)) +
                geom_bar(stat = "identity", position = "dodge") +
                xlab("Number of occurrences") +
                ylab("Number of articles") +
                scale_fill_discrete(name = "Search Term")
        
            plot <- ggplotly(stats_count)
            attr(plot,"text") <- paste0("Occurrences of '", to_look, "' in Text Fields") # nolint
            if (ly){
                plot
            } else {
                stats_count
            }
        } else {
            word_counts
        }
    }
}

#' Count the number of character in text and group into bins 
#'
#' @param df Dataframe which contains texts to analyze
#' @param nb_bins  Group the texts into bins of size nb_bins and count the number of texts in each bin
#' @param to_plot if True  returns a plot else a dataframe.
#' @param ly return a plotly  graph of a classic ggplot.
#' @export
count_character_bins <- function(df,
                      nb_bins = 10,
                      to_plot = TRUE,
                      ly = TRUE
                    ) {
    if (!requireNamespace("ggplot2", quietly = TRUE)) {
        stop("Package \"ggplot2\" needed for this function to work. Please install it.",
        call. = FALSE
        )
    }
    if (!requireNamespace("plotly", quietly = TRUE)) {
        stop("Package \"plotly\" needed for this function to work. Please install it.",
        call. = FALSE
        )
    }
    library(ggplot2)
    library(plotly)
    
    df <- data.frame(df)
    df <- df %>%
        mutate(Chars = nchar(Text))
 
    # Group the texts into bins of size nb_bins and count the number of texts in each bin
    df_summary <- df %>%
        mutate(Bin = cut(Chars, breaks = nb_bins)) %>%
        group_by(Bin) %>%
        summarize(Count = n())
    

    df_summary <- df_summary[order(as.numeric(gsub("\\((.*),(.*?)\\]", "\\1", df_summary$Bin))), ]
    df_summary$Bin <- format(paste(as.character(as.numeric(gsub("\\((.*),(.*?)\\]", "\\1", df_summary$Bin))), as.character(as.numeric(gsub("\\((.*),(.*?)\\]", "\\2", df_summary$Bin))), sep=" - ") 
    , big.mark = " ", scientific = FALSE)

    if (to_plot){
        sum <- ggplot(df_summary, aes(x = Bin, y = Count)) +
            geom_bar(stat = "identity") +
            xlab("Number of Characters") +
            ylab("Count") +
            ggtitle("Number of Characters in Texts") 

        plot <- ggplotly(sum) %>% layout(annotations = list(list(x = 0.5 , y = 1.1, text = "Number of Characters in Texts",
                                                                        showarrow = F, xref='paper', yref='paper')))
        attr(plot,"text") <- "Number of Characters in Texts"
        if (ly){
            plot 
        } else {
            sum
        }
    } else {
        df_summary
    }
} 



#' Apply stats ont the date on the dataframe 
#'
#' @param df Dataframe which contains texts to analyze
#' @param query_corpora corpora to be proceed.
#' @param to_plot if True  returns a plot else a dataframe.
#' @param over_all Result over all result for this collection.
#' @param ly return a plotly  graph of a classic ggplot.
#' @export
stats_date <- function(df,
                      query_corpora,
                      to_plot = TRUE,
                      over_all = FALSE,
                      ly = TRUE
                    ) {

    if (!requireNamespace("ggplot2", quietly = TRUE)) {
        stop("Package \"ggplot2\" needed for this function to work. Please install it.",
        call. = FALSE
        )
    }
    if (!requireNamespace("plotly", quietly = TRUE)) {
        stop("Package \"plotly\" needed for this function to work. Please install it.",
        call. = FALSE
        )
    }
    if (!requireNamespace("jsonlite", quietly = TRUE)) {
        stop("Package \"jsonlite\" needed for this function to work. Please install it.",
        call. = FALSE
        )
    }
    library(ggplot2)
    library(plotly)
    library(jsonlite)

    df <- data.frame(df) 
    df$Date <- substr(df$Date, 1, 4)
    df$year <- df$Date

    counts <- data.frame(table(df$year))

    if (over_all){


        corpora_stats <- jsonlite::stream_in(file(("https://gitlab.com/enpchina/histtext-r-client/-/raw/main/data-raw/stats_corpora.json")))
        df_list <- list()
        j <- 1
        for (entry in names(corpora_stats)) {
            # extract data for current entry

            dfs <- data.frame()

            for (n in names(corpora_stats[[entry]])) {
            dfs <- rbind(dfs, data.frame(Var1 = n, Freq = corpora_stats[[entry]][n][[1]][j]))
            }
            j <- j + 1

            # add current entry dataframe to list
            df_list[[entry]] <- dfs
        }
        if (query_corpora %in% names(df_list)) {
            stats_corpora <- df_list[[query_corpora]]
            df_merge <- merge(counts,stats_corpora, by = "Var1", all = TRUE)
            df_merge <- df_merge %>% mutate(Freq.x = ifelse(is.na(Freq.x), 0, Freq.x))

            df_merge <- df_merge %>%
            rename(Date = Var1)
            df_merge$Date <- as.numeric(as.character(df_merge$Date))
            df_merge <- df_merge[order(df_merge$Date, na.last = TRUE),]

            df_merge <- df_merge %>%
                mutate(Percentage = Freq.x / Freq.y )  # Calculate percentage

            if (to_plot){
                focused_point <- min(df_merge$Date)

                stats_corpora_date <- ggplot(data = df_merge, aes(x = Date, y = Percentage)) +
                    #geom_smooth(color = "blue", se = FALSE) +
                    geom_line(color = "blue") +  # Use geom_line instead of geom_col for a curve
                    geom_point(color = "blue") +  # Add points for each data point
                    geom_segment(aes(x = focused_point, y = 0, xend = focused_point, yend = df_merge$Percentage[df_merge$Date == focused_point]),
                        color = "red", linetype = "dashed") +
                    geom_segment(aes(x = min(df_merge$Date), y = df_merge$Percentage[df_merge$Date == focused_point], xend = focused_point, yend = df_merge$Percentage[df_merge$Date == focused_point]),
                        color = "red", linetype = "dashed") +
                    xlab("Year") +
                    ylab("Percentage") +
                    scale_y_continuous(labels = scales::percent) 

                plot <- ggplotly(stats_corpora_date) %>%
                    layout(annotations = list(
                        list(
                        x = 0.5,
                        y = 1.1,
                        text = "Frequency Percentage by Year over all results",
                        showarrow = FALSE,
                        xref = 'paper',
                        yref = 'paper'
                        )
                    ))

                attr(plot,"text") <- "Frequency Percentage by Year over all results"
                if (ly) {
                    plot
                } else {
                    stats_corpora_date
                }
            } else {
                df_merge
            }
        }

    } else {
    
        if (to_plot){
            stats_date <- ggplot(counts, aes(x = Var1, y = Freq)) +
                geom_bar(stat = "identity") +
                labs(x = "Year", y = "Number of results") 
            plot <- ggplotly(stats_date) %>% layout(annotations = list(list(x = 0.5 , y = 1.1, text = "Results by Year",
                                                                  showarrow = F, xref='paper', yref='paper')))

            attr(plot,"text") <- "Results by Year"
            if (ly) {
                plot
            } else {
               stats_date
            }
            
        } else {
            counts
        }
        

    }
}

#' Apply DTM on the date on the dataframe 
#'
#' @param df Dataframe which contains texts to analyze
#' @param top number of top words to get.
#' @param to_plot if True  returns a plot else a dataframe.
#' @param over_time Result of top words over time.
#' @param doc_simi Similarity between document.
#' @param ly return a plotly  graph of a classic ggplot.
#' @export
classic_dtm <- function(df,
                      top = 20,
                      to_plot = TRUE,
                      ly = TRUE,
                      over_time = FALSE,
                      doc_simi = FALSE
                    ) {
    if (!requireNamespace("ggplot2", quietly = TRUE)) {
        stop("Package \"ggplot2\" needed for this function to work. Please install it.",
        call. = FALSE
        )
    }
    if (!requireNamespace("plotly", quietly = TRUE)) {
        stop("Package \"plotly\" needed for this function to work. Please install it.",
        call. = FALSE
        )
    }
    if (!requireNamespace("quanteda", quietly = TRUE)) {
        stop("Package \"quanteda\" needed for this function to work. Please install it.",
        call. = FALSE
        )
    }
    if (!requireNamespace("stopwords", quietly = TRUE)) {
        stop("Package \"stopwords\" needed for this function to work. Please install it.",
        call. = FALSE
        )
    }
    library(ggplot2)
    library(plotly)
    library(quanteda)
    library(quanteda.textstats)
    library(stopwords)


    txt <- with(df, setNames(as.character(Text), DocId))
    tok_dfm <- tokens(txt, remove_punct = TRUE, remove_symbols = TRUE, remove_numbers=TRUE) %>%
        tokens_remove(append(append(stopwords("en",source="stopwords-iso"),
            stopwords("fr",source="stopwords-iso")),
            stopwords("zh",source="stopwords-iso"))) %>%
        dfm()


    freqs <- quanteda::colSums(tok_dfm)
    words <- colnames(tok_dfm)
    wordlist <- data.frame(words, freqs)
    wordIndexes <- order(wordlist[, "freqs"], decreasing = TRUE)
    wordlist <- wordlist[wordIndexes, ]

    
    if (over_time) {
        terms_to_observe <- wordlist$words[1:top] 


        textdata2 <- df 

        textdata2$year <- substr(textdata2$Date, 0, 4)
        textdata2$decade <- substr(textdata2$Date, 0, 4)


        DTM_reduced <- as.matrix(tok_dfm[, terms_to_observe])

        counts_per_decade <- aggregate(DTM_reduced, by = list(decade = textdata2$decade), sum)

        frequencies_long <- counts_per_decade %>%
            pivot_longer(cols = -decade, names_to = "word", values_to = "frequency")

        if (to_plot){

        
            # Create the plotly line plot
            plot <- ggplot(frequencies_long, aes(x = decade, y = frequency, color = word,group = word)) + 
                    geom_line() +
                    labs(
                        title = "Frequency of top Words over years",
                        x = "Decade",
                        y = "Frequency"
                    )


            # Convert ggplot to Plotly plot
            plotly_plot <- ggplotly(plot) %>% layout(annotations = list(list(x = 0.5 , y = 1.1, text = "Frequency of top Words over years",
                                                                        showarrow = F, xref='paper', yref='paper')))
            attr(plotly_plot,"text") <- "Frequency of top Words over years"
            if (ly){
                plotly_plot
            } else {
                plot
            }
        } else {
            frequencies_long
        }


    } else if (doc_simi) {
        library(ggdendro)
        simi <- textstat_simil(tok_dfm, tok_dfm, method = "cosine")
        simi <- as.matrix(simi)

        
        if (to_plot){
            hc <- hclust(as.dist(simi))
            p <- ggdendrogram(hc, rotate = FALSE, size = 2)
            plotly_plot <- ggplotly(p) %>% layout(annotations = list(list(x = 0.5 , y = 1.1, text = "Doc Similarity",
                                                                    showarrow = F, xref='paper', yref='paper')))
            attr(plotly_plot,"text") <- "Doc Similarity"
            if (ly){
                plotly_plot
            } else {
                p
            }
        } else {
            simi
        }

    } else {

        subset_wordlist <- head(wordlist, top)

        subset_wordlist <- subset_wordlist[order(subset_wordlist$freqs, decreasing = TRUE), ]

        subset_wordlist$words <- factor(subset_wordlist$words, levels = subset_wordlist$words)

        plot <- ggplot(subset_wordlist,aes(x = words, y = freqs)) +
                    geom_bar(stat = "identity") +
                    labs(
                        title = "Top Words",
                        x = list(title = "Words"),
                        y = list(title = "Frequency")
                    )
            
        plotly_plot <- ggplotly(plot) %>% layout(annotations = list(list(x = 0.5 , y = 1.1, text = "Top Words all",
                                                                    showarrow = F, xref='paper', yref='paper')))
        attr(plotly_plot,"text") <- "Top 25 Words"

        if (to_plot){
            if (ly) {
                plotly_plot
            } else {
                plot
            }
        } else {
            subset_wordlist
        }
    }
}