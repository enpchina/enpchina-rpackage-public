#' List available collections in SolR
#'
#' each collection should correspond to a corpus
#'
#' @return a vector of strings, listing the corpora
#' @export
list_corpora <- function(verbose = FALSE) {
  x <- query_server_get("/corpora", verbose = verbose)
  json <- httr::content(x, as = "text", encoding = "utf-8")
  jsonlite::fromJSON(json)
}

#' List possible search fields for a given corpus
#'
#' These fields are useful for advanced search functions
#' such as search_concordance_ex
#'
#' @param corpus the name of the corpus
#' @return a vector of strings, listing the search fields
#' @export
list_search_fields <- function(corpus, verbose = FALSE) {
  x <- query_server_get(stringr::str_c("/", corpus,
                                       "/list_searchfields",
                                       sep = ""),
                        verbose = verbose)
  json <- httr::content(x, as = "text", encoding = "utf-8")
  jsonlite::fromJSON(json)
}


#' List possible filter fields for a given corpus
#'
#' These fields are useful for advanced search functions
#' such as search_concordance_ex
#'
#' @param corpus the name of the corpus
#' @return a vector of strings, listing the search fields
#' @export
list_filter_fields <- function(corpus, verbose = FALSE) {
  x <- query_server_get(stringr::str_c("/", corpus,
                                       "/list_filterfields", sep = ""),
                        verbose = verbose)
   json <- httr::content(x, as = "text", encoding = "utf-8")
   jsonlite::fromJSON(json)
}


#' List possible filter values for a given filter field
#'
#' @param corpus the name of the corpus
#' @param filter_field the name of the filter field (see list_filter_fields())
#' @export
list_possible_filters <- function(corpus, filter_field, verbose = FALSE) {
  x <- xml2::read_xml(query_server_get(
    stringr::str_c("/", corpus, "/list_possible_filters",
      "/", RCurl::curlEscape(filter_field),
      sep = ""
    )
  ), verbose = verbose)
  filters <- xml2::xml_find_all(x, ".//Filter")
  Value <- xml2::xml_attr(filters, "value")
  N <- xml2::xml_attr(filters, "n")
  table <- dplyr::as_tibble(cbind(Value, N))
  dplyr::mutate(table, N = as.integer(N))
}


#' Check if a corpus accepts date queries
#'
#' @param corpus the name of the corpus
#' @return TRUE if the corpus accepts date queries, FALSE otherwise
#' @export
accepts_date_queries <- function(corpus, verbose = FALSE) {
  x <- query_server_get(stringr::str_c("/", corpus,
                                       "/date_support", sep = ""),
                        verbose = verbose)
  json <- httr::content(x, as = "text", encoding = "utf-8")
  jsonlite::fromJSON(json)
}


query_builder <- function(q, operator=" OR ") {
  if (length(q) == 1) {
    return(q)
  }

  query <- paste(q, collapse = operator)
  paste0("(", query, ")")
}


#' KWIC Search In ENP Corpora
#'
#' Simple Keyword search in ENP's corpora
#'
#' @param q a query string or a vector of query strings
#' @param corpus the name of the corpus (currently available : "proquest", "shunpao", "imh-en")
#' @param context_size length of the text snippets to be retrieved
#' @param verbose TRUE to print queries sent to the server, FALSE otherwise
#'   (default).
#' @return a tidy table with the result of the concordance
#' @examples
#' conc = search_enp_corpus('"蔣介石"')
#' View(conc)
#' @export
search_concordance <- function(q, corpus="imh-en", search_fields=c(),
                                  context_size=30, start=0,
                                  dates=c(), filter_query=c(),
                                  search_operator="OR",
                                  filter_operator="AND",
                                  not_filters=c(),
                                  step_size=1000,
                                  no_repeat=FALSE,
                                  maxrows=NULL,
                                  verbose=FALSE) {
  search_concordance_ex(q, corpus=corpus, search_fields=search_fields,
                                  context_size=context_size, start=start,
                                  dates=dates, filter_query=filter_query,
                                  search_operator=search_operator,
                                  filter_operator=filter_operator,
                                  not_filters=not_filters,
                                  step_size=step_size,
                                  no_repeat=no_repeat,
                                  maxrows=maxrows,
                                  verbose=verbose)
}


#' Extended KWIC Search In ENP Corpora
#'
#' Keyword search in ENP's corpora on one or more search fields.
#'
#' @param q a query string or a vector of query strings
#' @param corpus the name of the corpus (see list_corpora() for possible
#'   choices)
#' @param search_fields a vector of search field strings (see
#'   list_search_fields() for possible choices). If left empty, defaults to all
#'   possible search fields.
#' @param context_size length of the text snippets to be retrieved
#' @param start the first index to consider when retrieving results
#' @param dates a vector of dates and date ranges. Dates must be in the
#'   YYYY-MM-DD, YYYY-MM or YYYY formats. Date ranges can also be requested
#'   using the following syntax: [Date1;Date2] or [Date1 TO Date2]. Date1
#'   (Date2) can be replaced with the '*' symbol in order to request no lower
#'   (upper) date limit.
#' @param filter_query a named vector where the names are filter fields (see
#'   list_filter_fields()) and the values are the filter queries. Only documents
#'   that exactly match a given filter value will be returned. The '*' symbol
#'   can be used as a wildcard symbol.
#' @param search_operator the logical operator to use between search field
#'   queries. Possible operators: OR, AND (default: OR).
#' @param filter_operator the logical operator to use between filter queries.
#'   Possible operators: OR, AND (default: AND).
#' @param not_filters vector of filter field names for which the filter must NOT
#'   match.
#' @param verbose TRUE to print queries sent to the server, FALSE otherwise
#'   (default).
#' @return a tidy table with the result of the concordance
#' @export
search_concordance_ex <- function(q, corpus="imh-en", search_fields=c(),
                                  context_size=30, start=0,
                                  dates=c(), filter_query=c(),
                                  search_operator="OR",
                                  filter_operator="AND",
                                  not_filters=c(),
                                  step_size=1000,
                                  no_repeat=FALSE,
                                  maxrows=NULL,
                                  verbose=FALSE) {
  query <- RCurl::curlEscape(query_builder(q))
  if (is.null(search_fields)) {
    search_fields <- list_search_fields(corpus)
  }
  query_params <- list()
  for (search_field in search_fields) {
    ## query_sep <- if (query_params == "") "sf=" else "&sf="
    ## query_params <- paste(query_params,
    ##                       query_sep, RCurl::curlEscape(search_field),
    ##                       sep = "")
    query_params <- c(query_params, sf = search_field)
  }
  for (date in dates) {
    ## query_params <- paste(query_params, "&date=",
    ##                       RCurl::curlEscape(date), sep = "")
    query_params <- c(query_params, date = date)
  }
  for (filter_name in names(filter_query)) {
    ## query_params <- paste(
    ##   query_params, "&fq=",
    ##   RCurl::curlEscape(paste(
    ##            if (filter_name %in% not_filters)
    ##              paste0("!", filter_name)
    ##            else filter_name, ":(",
    ##            filter_query[filter_name],
    ##            ")",
    ##            sep = "")),
    ##   sep = "")
    query_params <- c(query_params, fq = paste(
      if (filter_name %in% not_filters) {
        paste0("!", filter_name)
      } else {
        filter_name
      }, ":(",
      filter_query[filter_name],
      ")",
      sep = ""
    ))
  }
  ## query_params <- paste(query_params, "&sfop=", search_operator, sep = "")
  query_params <- c(query_params, sfop = search_operator)
  ## query_params <- paste(query_params, "&fqop=", filter_operator, sep = "")
  query_params <- c(query_params, fqop = filter_operator)
  ## query_params <- paste(query_params, "&ctxt=", context_size, sep = "")
  query_params <- c(query_params, ctxt = context_size)
  ## query_params <- paste(query_params, "&maxrows=",
  ##                       format(step_size, scientific = FALSE), sep = "")
  query_params <- c(query_params, maxrows = format(step_size,
                                                   scientific = FALSE))

  result <- NULL
  repeat {
    query_url <- stringr::str_c("/", corpus, "/rsearch/", query,
                                ## "?", query_params,
                                ## "&start=", format(start, scientific = FALSE),
                                sep = "")
    current_query <- c(query_params, start = format(start, scientific = FALSE))
    x <- xml2::read_xml(query_server_get(query_url, query = current_query,
                                         verbose = verbose),
                        options = "HUGE")
    matches <- xml2::xml_find_all(x, ".//match")
    Before <- xml2::xml_text(xml2::xml_find_all(matches, "./Before"))
    After <- xml2::xml_text(xml2::xml_find_all(matches, "./After"))
    DocId <- xml2::xml_text(xml2::xml_find_all(matches, "DocId"))
    Date <- xml2::xml_text(xml2::xml_find_all(matches, "Date"))
    Title <- xml2::xml_text(xml2::xml_find_all(matches, "./Title"))
    Source <- xml2::xml_text(xml2::xml_find_all(matches, "./Source"))
    Matched <- xml2::xml_text(xml2::xml_find_all(matches, "./Matched"))
    batch_result <- dplyr::as_tibble(cbind(DocId, Date, Title,
                                           Source, Before,
                                           Matched, After))

    if (dim(batch_result)[1] == 0 || no_repeat) {
      if (is.null(result)) result <- batch_result
      break
    }
    if (is.null(result) || dim(result)[1] == 0) result <- batch_result
    else result <- dplyr::bind_rows(result, batch_result)
    start <- start + step_size
  }

  result
}


#'
#' Search for documents
#'
#' @param q a query string or a vector of query strings
#' @param corpus the name of the corpus (currently available : "proquest",
#'   "shunpao", "imh-en")
#' @param verbose TRUE to print queries sent to the server, FALSE otherwise
#'   (default).
#' @return a tidy table with the documents IDs and basic metadata
#' @export
search_documents <- function(q, corpus="imh-en",
                                search_fields=c(), start=0,
                                dates=c(), filter_query=c(),
                                search_operator="OR",
                                filter_operator="AND",
                                not_filters=c(),
                                step_size=1000,
                                no_repeat=FALSE,
                                maxrows=NULL,
                                verbose=FALSE) {
  search_documents_ex(q, corpus=corpus,
                        search_fields=search_fields, start=start,
                        dates=dates, filter_query=filter_query,
                        search_operator=search_operator,
                        filter_operator=filter_operator,
                        not_filters=not_filters,
                        step_size=step_size,
                        no_repeat=no_repeat,
                        maxrows=maxrows,
                        verbose=verbose)
}

#'
#' Extended Search for documents
#'
#' @param q a query string or a vector of query strings
#' @param corpus the name of the corpus (currently available : "proquest",
#'   "shunpao", "imh-en")
#' @param search_fields a vector of search field strings (see
#'   list_search_fields() for possible choices). If left empty, defaults to all
#'   possible search fields.
#' @param start the first index to consider when retrieving results
#' @param dates a vector of dates and date ranges. Dates must be in the
#'   YYYY-MM-DD, YYYY-MM or YYYY formats. Date ranges can also be requested
#'   using the following syntax: [Date1;Date2] or [Date1 TO Date2]. Date1
#'   (Date2) can be replaced with the '*' symbol in order to request no lower
#'   (upper) date limit.
#' @param filter_query a named vector where the names are filter fields (see
#'   list_filter_fields()) and the values are the filter queries. Only documents
#'   that exactly match a given filter value will be returned. The '*' symbol
#'   can be used as a wildcard symbol.
#' @param search_operator the logical operator to use between search field
#'   queries. Possible operators: OR, AND (default: OR).
#' @param filter_operator the logical operator to use between filter queries.
#'   Possible operators: OR, AND (default: AND).
#' @param not_filters vector of filter field names for which the filter must NOT
#'   match.
#' @param verbose TRUE to print queries sent to the server, FALSE otherwise
#'   (default).
#' @return a tidy table with the documents IDs and basic metadata
#' @export
search_documents_ex <- function(q, corpus="imh-en",
                                search_fields=c(), start=0,
                                dates=c(), filter_query=c(),
                                search_operator="OR",
                                filter_operator="AND",
                                not_filters=c(),
                                step_size=1000,
                                no_repeat=FALSE,
                                maxrows=NULL,
                                verbose=FALSE) {
  query <- RCurl::curlEscape(query_builder(q))
  if (is.null(search_fields)) {
    search_fields <- list_search_fields(corpus)
  }
  ## query_params <- ""
  ## for (search_field in search_fields) {
  ##   query_sep <- if (query_params == "") "sf=" else "&sf="
  ##   query_params <- paste(query_params,
  ##                         query_sep, RCurl::curlEscape(search_field),
  ##                         sep = "")
  ## }
  ## for (date in dates) {
  ##   query_params <- paste(query_params, "&date=",
  ##                         RCurl::curlEscape(date), sep = "")
  ## }
  ## for (filter_name in names(filter_query)) {
  ##   query_params <- paste(
  ##     query_params, "&fq=",
  ##     RCurl::curlEscape(paste(
  ##              if (filter_name %in% not_filters)
  ##                paste0("!", filter_name)
  ##              else filter_name, ":(",
  ##              filter_query[filter_name],
  ##              ")",
  ##              sep = "")),
  ##     sep = "")
  ## }
  ## query_params <- paste(query_params, "&sfop=", search_operator, sep = "")
  ## query_params <- paste(query_params, "&fqop=", filter_operator, sep = "")
  ## query_params <- paste(query_params, "&maxrows=",
  ##                       format(step_size, scientific = FALSE),
  ##                       sep = "")

  query_params <- list()
  for (search_field in search_fields) {
    query_params <- c(query_params, sf = search_field)
  }
  for (date in dates) {
    query_params <- c(query_params, date = date)
  }
  for (filter_name in names(filter_query)) {
    query_params <- c(query_params, fq = paste(
      if (filter_name %in% not_filters) {
        paste0("!", filter_name)
      } else {
        filter_name
      }, ":(",
      filter_query[filter_name],
      ")",
      sep = ""
    ))
  }
  query_params <- c(query_params, sfop = search_operator)
  query_params <- c(query_params, fqop = filter_operator)
  query_params <- c(query_params, maxrows = format(step_size,
                                                   scientific = FALSE))

  result <- NULL
  repeat {
    query_url <- stringr::str_c("/", corpus, "/rsearch/", query,
                                ## "?", query_params,
                                ## "&start=", format(start, scientific = FALSE),
                                sep = "")
    current_query <- c(query_params, start = format(start, scientific = FALSE))
    x <- xml2::read_xml(query_server_get(query_url, query = current_query,
                                         verbose = verbose),
                        options = "HUGE")
    matches <- xml2::xml_find_all(x, ".//match")
    DocId <- xml2::xml_text(xml2::xml_find_all(matches,"DocId"))
    Date <- xml2::xml_text(xml2::xml_find_all(matches, "Date"))
    Title <- xml2::xml_text(xml2::xml_find_all(matches, "./Title"))
    Source <- xml2::xml_text(xml2::xml_find_all(matches, "./Source"))
    batch_result <- dplyr::as_tibble(cbind(DocId, Date, Title, Source))

    if (dim(batch_result)[1] == 0 || no_repeat) {
      if (is.null(result)) result <- batch_result
      break
    }
    if (is.null(result) || dim(result)[1] == 0) result <- batch_result
    else result <- dplyr::bind_rows(result, batch_result)
    start <- start + step_size
  }
  result
}


#' Count the number of documents that can be returned by a query
#'
#' @param q a query string or a vector of query strings
#' @param corpus the name of the corpus
#' @param search_fields a vector of search field strings (see
#'   list_search_fields() for possible choices). If left empty, defaults to all
#'   possible search fields.
#' @param dates a vector of dates and date ranges. Dates must be in the
#'   YYYY-MM-DD, YYYY-MM or YYYY formats. Date ranges can also be requested
#'   using the following syntax: [Date1;Date2] or [Date1 TO Date2]. Date1
#'   (Date2) can be replaced with the '*' symbol in order to request no lower
#'   (upper) date limit.
#' @param filter_query a named vector where the names are filter fields (see
#'   list_filter_fields()) and the values are the filter queries. Only documents
#'   that exactly match a given filter value will be returned. The '*' symbol
#'   can be used as a wildcard symbol.
#' @param search_operator the logical operator to use between search field
#'   queries. Possible operators: OR, AND (default: OR).
#' @param filter_operator the logical operator to use between filter queries.
#'   Possible operators: OR, AND (default: AND).
#' @param not_filters vector of filter field names for which the filter must NOT
#'   match.
#' @param verbose TRUE to print queries sent to the server, FALSE otherwise
#'   (default).
#' @return an integer
#' @export
count_search_documents <- function(q, corpus = "imh-en",
                                   search_fields=c(),
                                   dates=c(), filter_query=c(),
                                   search_operator="OR",
                                   filter_operator="AND",
                                   not_filters=c(),
                                   verbose=FALSE) {
  query <- RCurl::curlEscape(query_builder(q))
  if (is.null(search_fields)) {
    search_fields <- list_search_fields(corpus)
  }
  query_params <- list()
  for (search_field in search_fields) {
    query_params <- c(query_params, sf = search_field)
  }
  for (date in dates) {
    query_params <- c(query_params, date = date)
  }
  for (filter_name in names(filter_query)) {
    query_params <- c(query_params, fq = paste(
      if (filter_name %in% not_filters) {
        paste0("!", filter_name)
      } else {
        filter_name
      }, ":(",
      filter_query[filter_name],
      ")",
      sep = ""
    ))
  }
  query_params <- c(query_params, sfop = search_operator)
  query_params <- c(query_params, fqop = filter_operator)
  query_url <- stringr::str_c("/", corpus, "/rsearch_count/", query,
                              ## "?", query_params,
                              sep = "")
  x <- query_server_get(query_url, query = query_params, verbose = verbose)
  as.integer(httr::content(x, as = "text", encoding = "utf-8"))
}


#'
#' Get the number of articles matching a query, by date
#'
#' @param query a query string or a vector of query strings
#' @param corpus the name of the corpus
#' @param search_fields a vector of search field strings (see
#'   list_search_fields() for possible choices). If left empty, defaults to all
#'   possible search fields.
#' @param search_operator the logical operator to use between search field
#'   queries. Possible operators: OR, AND (default: OR).
#' @param by_field the field to use in order to group and count documents. See
#'   `list_filter_fields()` for good field candidates. Defaults to grouping by
#'   date.
#' @param date_start if grouping by date, the start date to consider (YYYY-MM-DD
#'   format)
#' @param date_end if grouping by date, the end date (not included) to consider
#'   (YYYY-MM-DD format)
#' @param date_gap_type if grouping by date, gap type to consider among DAY,
#'   MONTH and YEAR.
#' @param date_gap if grouping by date, the number of GAP_TYPE to apply between
#'   each date range.
#' @return a tidy table with the documents IDs and basic metadata
#' @export
count_documents <- function(query, corpus, search_fields=c(),
                            search_operator="OR",
                            by_field="date",
                            date_start=NULL, date_end=NULL,
                            date_gap_type="YEAR",
                            date_gap=1,
                            verbose = FALSE) {
  query <- RCurl::curlEscape(query_builder(query))
  if (is.null(search_fields)) {
    search_fields <- c("__default__")
  }
  query_params <- list()
  for (search_field in search_fields) {
    query_params <- c(query_params, sf = search_field)
  }
  query_params <- c(query_params, sfop = search_operator)
  query_params <- c(query_params, by_field = by_field)
  if (by_field == "date" && !is.null(date_start) && !is.null(date_end)) {
    query_params <- c(query_params, date_start = date_start)
    query_params <- c(query_params, date_end = date_end)
    query_params <- c(query_params, date_gap_type = date_gap_type)
    query_params <- c(query_params, date_gap = date_gap)
  }
  x <- xml2::read_xml(query_server_get(stringr::str_c(
                                                  "/", corpus,
                                                  "/countdocs/",
                                                  query,
                                                  sep = ""),
                                       query = query_params,
                                       verbose = verbose))
  counts <- xml2::xml_find_all(x, ".//Count")
  GroupField <- xml2::xml_attr(counts, by_field)
  N <- xml2::xml_attr(counts, "n")
  bound <- cbind(GroupField, N)
  if (by_field == "date") {  # Keeps compatibility with older versions
    colnames(bound) <- c("Date", "N")
  }
  table <- dplyr::as_tibble(bound)
  dplyr::mutate(table, N = as.integer(N))
}


#'
#' Retrieve document from ID
#'
#' @param docs a dataframe which includes an `DocId` column
#' @param corpus the name of the corpus (currently available : "proquest",
#'   "shunpao", "imh-en")
#' @return a tidy table with the document as a row
#' @export
get_documents <- function(docs, corpus="imh-en",
                          batch_size = 10, verbose = TRUE) {
  url <- stringr::str_c("/", corpus, "/getdocuments", sep = "")
  ids <- if ("DocId" %in% colnames(docs)) docs$DocId else docs$Id
  nb_ids <- length(ids) - 1
  step_size <- batch_size
  result <- NULL
  for (step_idx in 0:(floor(nb_ids / step_size))) {
    start <- step_idx * step_size + 1
    end <- start + step_size - 1
    if (verbose) print(start)
    if (length(na.omit(ids[start:end])) == 0) {
      warning("Encountered only NA ids in get_documents - please inform package maintainer")
      next
    }
    body <- stringr::str_c(na.omit(ids[start:end]), collapse = "\n")
    resp <- query_server_post(
      url,
      httr::add_headers(`Content-Type` = "text/plain"),
      body = body
    )
    batch <- readr::read_csv(httr::content(resp, "text", encoding = "utf-8"),
                             col_types = readr::cols(.default = readr::col_character()))
    if (is.null(result) || dim(result)[1] == 0) result <- batch
    else if (dim(batch)[1] > 0) result <- dplyr::bind_rows(result, batch)
  }
  result
}


fields_are_valid <- function(fields, corpus) {
  valid_fields <- c(list_search_fields(corpus), list_filter_fields(corpus))
  if (accepts_date_queries(corpus)) {
    valid_fields <- append(valid_fields, "date")
  }
  all(fields %in% valid_fields)
}

#' Retrieve the content associated with each search field
#'
#' @param docs a dataframe which includes an `DocId` column
#' @param corpus the name of the corpus (see list_corpora())
#' @param search_fields a vector of search field strings (see
#'   list_search_fields() for possible choices). If left empty, defaults to all
#'   possible search fields. If corpus supports dates, "date" is also a valid
#'   field.
#' @return a tidy table with the search fields content in rows and search fields
#'   as columns.
#' @export
get_search_fields_content <- function(docs, corpus = "imh-en",
                                      search_fields = c(),
                                      batch_size = 10,
                                      verbose = TRUE) {
  if (is.null(search_fields)) {
    search_fields <- list_search_fields(corpus)
  }

  if (!fields_are_valid(search_fields, corpus)) {
    stop("Some requested fields do not exist for this corpus. Use `list_search_fields` and `list_filter_fields` to retrieve lists of valid fields.")
  }

  url_searchfields <- RCurl::curlEscape(paste(search_fields, collapse = "#"))
  url <- stringr::str_c("/", corpus, "/getdocuments",
                        "/", url_searchfields,
                        sep = "")

  ids <- if ("DocId" %in% colnames(docs)) docs$DocId else docs$Id
  nb_ids <- length(ids) - 1
  step_size <- batch_size
  result <- NULL
  for (step_idx in 0:(floor(nb_ids / step_size))) {
    start <- step_idx * step_size + 1
    end <- start + step_size - 1
    if (verbose) print(start)
    if (length(na.omit(ids[start:end])) == 0) {
      warning("Encountered only NA ids in get_documents - please inform package maintainer")
      next
    }
    body <- stringr::str_c(na.omit(ids[start:end]), collapse = "\n")
    resp <- query_server_post(
      url,
      httr::add_headers(`Content-Type` = "text/plain"),
      body = body
    )
    batch <- readr::read_csv(httr::content(resp, "text", encoding = "utf-8"),
                             col_types = readr::cols(.default = readr::col_character()))
    if (is.null(result) || dim(result)[1] == 0) result <- batch
    else if (dim(batch)[1] > 0) result <- dplyr::bind_rows(result, batch)
  }
  result
}


#' View a single document in RStudio
#'
#' @param docid id of the document to view
#' @param corpus the name of the corpus (see list_corpora())
#' @param query the query to highlight in the viewer -- may be a query string or a vector of query strings
#' @export
view_document <- function(docid, corpus, query=c()) {
  concordance <- c()
  if (!is.null(query)) {
    concordance <- search_concordance_ex(query, corpus = corpus,
                                         filter_query = list(id = docid))
  }

  document <- get_documents(dplyr::tibble(DocId = c(docid)),
                            corpus = corpus, verbose = FALSE)

  text <- document[1, ]$Text
  title <- document[1, ]$Title
  date <- document[1, ]$Date
  source <- document[1, ]$Source

  for (i in rownames(concordance)) {
    text <- stringr::str_replace_all(text,
                                     paste("(", concordance[i, "Matched"], ")",
                                           sep = ""),
                                     '<span style="color: red;">\\1</span>')
  }

  lines <- c("<!DOCTYPE html>", "<html>",
             "<head>", '<meta charset="UTF-8">', "</head>",
             "<body>")
  lines <- c(lines, "<h2>", htmltools::htmlEscape(title), "</h2>",
             "Date: ", date, " - Source: ", source,
             "<br/>", "<br/>", "<br/>")
  lines <- c(lines, text)
  lines <- c(lines, "</body>", "</html>")

  tmp_html <- tempfile(fileext = ".html")
  file_conn <- file(tmp_html)
  writeLines(lines, file_conn)
  close(file_conn)

  rstudioapi::viewer(tmp_html)
}


#' Display an entry from ProQuest Corpus
#'
#' retrieve the full text from the id
#'
#' @param id document id (should look like "A107")
#' @return nothing
#' @export
proquest_view <- function(id) {
  getOption("viewer")(stringr::str_c("https://search.proquest.com.p.numerica-sinica.eu/docview/", id))
}
