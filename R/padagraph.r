library(dplyr)

show_url <- function(url) {
  getOption("viewer")(url)
}

as_graphml <- function(g) {
  tmp <- tempfile("pdggml")
  igraph::write_graph(g, tmp, "graphml")
  gml <- readr::read_file(tmp)
  unlink(tmp)
  gml
}

#' Send a tidygraph to padagraph and displays it
#'
#'
#' @param g a graph in tidy format
#' @param name the name to be given to the graph
#' @return nothing (will show the graph in the viewer panel)
#' @export
in_padagraph <- function(g, name) {
  # RCurl::postForm("https://pdg.enpchina.eu/post",url="none", gid=name, graph_data=as_graphml(g))
  # url <- stringr::str_c("https://pdg.enpchina.eu/rstudio?gid=", name, sep="", collapse = "")
  url <- get_padagraph_url(g, name)
  show_url(url)
}


#' Send a tidygraph to padagraph and return the URL
#' 
#' @param g a graph in tidy format
#' @param name the name to be given to the graph
#' @return the URL to the padagraph
#' @export
get_padagraph_url <- function(g, name) {
  RCurl::postForm("https://pdg.enpchina.eu/post",url="none", gid=name, graph_data=as_graphml(g))
  stringr::str_c("https://pdg.enpchina.eu/rstudio?gid=", name, sep="", collapse = "")
}


#' Save a tidygraph into a file
#'
#' @param graph the tidygraph to save
#' @param filepath the location and filename to use to save the graph
#' @export
save_graph <- function(graph, filepath) {
  save(graph, file = filepath)
}


#' Load and send a previously saved graph object into padagraph
#'
#' @param filepath the path to a file which contains a tidygraph (created with
#'   `save_graph`)
#' @param name the name to be given to the graph
#' @param show_graph if TRUE, show the graph in a RStudio viewer
#' @return the URL to the padagraph
#' @export
load_in_padagraph <- function(filepath, name, show_graph = TRUE) {
  graph <- load(filepath)
  url <- get_padagraph_url(graph, name)
  if (show_graph)
    show_url(url)
  url
}
