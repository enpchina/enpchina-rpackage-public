#' List available CWS models on the server
#'
#' @return a vector of strings listing the models
#' @export
list_cws_models <- function(verbose = FALSE) {
  x <- query_server_get("/cws/list_models", verbose = verbose)
  json <- httr::content(x, as = "text", encoding = "utf-8")
  jsonlite::fromJSON(json)
}

#' Get the name of the default CWS model for a given corpus
#'
#' @param corpus to get the information on
#' @return the name of the default CWS model
#' @export
get_default_cws_model <- function(corpus, verbose = FALSE) {
  x <- query_server_get(paste0("/cws/get_default_model/", corpus), verbose)
  json <- httr::content(x, as = "text", encoding = "utf-8")
  jsonlite::fromJSON(json)$model
}


split_text_nostride <- function(text, maxlen) {
  text_splits <- c()
  offsets <- c()
  current_offset <- 0
  while (nchar(text) != 0) {
    current_subtext <- substring(text, 1, maxlen)
    text_splits <- append(text_splits, current_subtext)
    offsets <- append(offsets, current_offset)
    current_offset <- current_offset + maxlen
    text <- substring(text, maxlen + 1)
  }
  list("subtexts" = text_splits, "offsets" = offsets)
}


split_text_for_cws <- function(text, maxlen = 2048) {
  to_split <- c(text)
  to_split_offsets <- c(0)
  result <- c()
  offsets <- c()

  while (length(to_split) != 0) {
    element <- to_split[1]
    offset <- to_split_offsets[1]
    to_split <- tail(to_split, -1)
    to_split_offsets <- tail(to_split_offsets, -1)

    if (nchar(element) <= maxlen) {
      result <- append(result, element)
      offsets <- append(offsets, offset)
      next
    }

    splits <- split_text_nostride(element, maxlen)
    result <- append(result, splits$subtexts)
    offsets <- append(offsets, splits$offsets + offset)
  }

  list("subtexts" = result, "offsets" = offsets)
}


#' Apply Chinese Word Segmentation on a string
#'
#' @param text to be analyzed
#' @param model to be used (use `list_cws_models()` to get available models)
#' @param detailed_output if TRUE, return a dataframe with one row per token
#'   (with positions and confidence scores)
#' @param token_separator the character to use to separate each token (default
#'   uses a normal space)
#' @export
run_cws <- function(text, model = "trftc_shunpao:zh:cws",
                    detailed_output = FALSE,
                    token_separator = " ",
                    verbose = FALSE) {
  url <- paste("/cws/", model, "/apply",
               sep = "")
  query <- list(detailed = if (detailed_output) "true" else "false",
                separator = token_separator)
  if (is.null(text) || is.na(text)) {
    warning("NULL or NA value found instead of text.")
    return(NULL)
  }
  maxlen <- 1024
  if (nchar(text) > maxlen) {
    warning("Text is longer than 1024 characters, applied a quick and dirty split.")
  }
  text_splits <- split_text_for_cws(text, maxlen = maxlen)
  result <- NULL
  for (split_idx in seq_along(text_splits$subtexts)) {
    subtext <- text_splits$subtexts[split_idx]
    offset <- text_splits$offsets[split_idx]
    resp <- query_server_post(
      url,
      httr::add_headers(`Content-Type` = "text/plain"),
      body = subtext,
      query = query,
      verbose = verbose
    )
    batch <- if (detailed_output) {
      readr::read_csv(
        httr::content(resp, "text", encoding = "utf-8"),
        col_types = readr::cols(
          .default = readr::col_character(),
          Start = readr::col_integer(),
          End = readr::col_integer(),
          Confidence = readr::col_double()
        )
      )
    } else {
      readr::read_csv(
        httr::content(resp, "text", encoding = "utf-8"),
        col_types = readr::cols(
          .default = readr::col_character(),
        )
      )
    }
    if (detailed_output && dim(batch)[1] > 0) {
      batch$Start <- batch$Start + offset
      batch$End <- batch$End + offset
    }
    if (is.null(result) || dim(result)[1] == 0) result <- batch
    else if (dim(batch)[1] > 0) result <- dplyr::bind_rows(result, batch)
  }

  unique(result)
}


#' Apply Chinese Word Segmentation on the specified column of a dataframe
#'
#' @param df Dataframe which contains texts to analyze
#' @param text_column to use in the dataframe which contains the input text.
#' @param id_column that is used to associate ids to the CWS outputs. By
#'   default, uses the row index in `df`.
#' @param model to be used (use `list_cws_models()` to get available models)
#' @param detailed_output if TRUE, return a dataframe with one row per token
#'   (with positions and confidence scores)
#' @param token_separator the character to use to separate each token (default
#'   uses a normal space)
#' @note If a given text is longer than 2048 characters, it will be split into
#'   smaller chunks. Preprocess texts beforehand if you need clean splits.
#' @export
cws_on_df <- function(df, text_column, id_column = NULL,
                      model = "trftc_shunpao:zh:cws",
                      detailed_output = FALSE,
                      token_separator = " ",
                      verbose = TRUE) {
  result <- NULL
  for (i in 1:nrow(df)) {
    if (verbose && i %% 10 == 1) {
      print(stringr::str_glue("{i}/{nrow(df)}"))
    }
    cws_output <- run_cws(toString(df[i, text_column]), model = model,
                          detailed_output = detailed_output,
                          token_separator = token_separator)
    if (is.null(cws_output)) {
      next
    }
    id <- if (is.null(id_column)) i else df[i, id_column]
    cws_output[, if (is.null(id_column)) "TextIndex" else id_column] <- id

    if (is.null(result) || dim(result)[1] == 0) result <- cws_output
    else if (dim(cws_output)[1] > 0)  result <- dplyr::bind_rows(result,
                                                                 cws_output)
  }
  result
}


#' Apply Chinese Word Segmentation on a corpus
#'
#' @param docIDs Dataframe of documents which includes an `DocId` column
#' @param corpus to be used
#' @param model to be used (the default model for the corpus is used if not
#'   specified)
#' @param field on which will be applied the model
#' @param detailed_output if TRUE, return a dataframe with one row per token
#'   (with positions and confidence scores)
#' @param token_separator the character to use to separate each token (default
#'   uses a normal space)
#' @export
cws_on_corpus <- function(docids, corpus = "shunpao",
                          model = "__default__",
                          field = "__default__",
                          detailed_output = FALSE,
                          token_separator = " ",
                          batch_size = 10,
                          verbose = TRUE) {
  url <- paste("/", corpus, "-cws/apply",
               sep = ""
               )
  query <- list(model = model,
                field = field,
                detailed = if (detailed_output) "true" else "false",
                separator = token_separator)

  ids <- if ("DocId" %in% colnames(docids)) docids$DocId else docids$Id

  nb_ids <- length(ids) - 1
  step <- batch_size
  result <- NULL
  for (start in 0:(floor(nb_ids / step))) {
    start <- start * step + 1
    end <- start + step - 1
    if (verbose)
      print(stringr::str_glue("{start}/{nb_ids + 1}"))
    if (length(na.omit(ids[start:end])) == 0) {
      warning("Encountered only NA ids in cws_on_corpus - please inform package maintainer")
      next
    }
    body <- stringr::str_c(na.omit(ids[start:end]), collapse = "\n")
    resp <- query_server_post(
      url,
      httr::add_headers(`Content-Type` = "text/plain"),
      body = body,
      query = query,
      verbose = FALSE
    )
    batch <- if (detailed_output) {
               readr::read_csv(
                 httr::content(resp, "text", encoding = "utf-8"),
                 col_types = readr::cols(
                   .default = readr::col_character(),
                   Start = readr::col_integer(),
                   End = readr::col_integer(),
                   Confidence = readr::col_double()
                 )
               )
             } else {
               readr::read_csv(
                 httr::content(resp, "text", encoding = "utf-8"),
                 col_types = readr::cols(
                   .default = readr::col_character(),
                 )
               )
             }
    if (is.null(result) || dim(result)[1] == 0) result <- batch
    else if (dim(batch)[1] > 0) result <- dplyr::bind_rows(result, batch)
  }
  result
}
