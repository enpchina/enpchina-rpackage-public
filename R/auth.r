

#' Sets the config file in order to specify the server URL to use (+ other
#' needed information).
#'
#' @param path the location of a config file to use. If NULL, `url` needs to be
#'   set instead.
#' @param url the server url which runs the server-side API.
#' @param user a username, if required by the server.
#' @param password a password, if required by the server.
#' @export
set_config_file <- function(path = NULL, domain = NULL,
                            user = NULL, password = NULL) {
  if (!is.null(path)) {
    file.copy(path, enp_config_path, overwrite = TRUE)
    return()
  }

  if (is.null(domain)) {
    stop("Error: path and domain are both NULL!")
  }

  yaml_config <- list(server = list(domain = domain))

  if (!is.null(user) && is.null(password) || is.null(user) && !is.null(password)) {
    stop("Error: user (or password) is set. Please also set the password (or user)!")
  }

  if (!is.null(user)) {
    yaml_config$server$user <- user
  }

  if (!is.null(password)) {
    yaml_config$server$password <- password
  }

  yaml::write_yaml(yaml_config, enp_config_path)
}


get_server_info <- function() {
  yaml::yaml.load_file(enp_config_path)$server
}


#' Retrieve the error status of a response.
#'
#' @param response a `httr::response` object
#' @return a boolean with a 'message' attribute
#' @export
get_error_status <- function(response) {
  if (is.null(response)) {
    has_error <- TRUE
    attr(has_error, "message") <- "Can't connect to the server. If your internet connection is functional, please contact your histtext server administrator."
    return(has_error)
  }
  if (response$status_code == 401 || response$status_code == 403) {
    has_error <- TRUE
    attr(has_error, "message") <- paste(response$status_code, "- Make sure that your username and password are properly configured in the histtext package.")
    return(has_error)
  }
  if (response$status_code == 404 || response$status_code == 400) {
    has_error <- TRUE
    json <- httr::content(response)
    message <- if (is.list(json) && !is.null(json$detail)) {
                 json$detail
               } else {
                 "Invalid query from the package. Please contact the histtext package maintainer."
               }
    attr(has_error, "message") <- paste(response$status_code, "-", message)
    return(has_error)
  }
  if (response$status_code == 500) {
    has_error <- TRUE
    attr(has_error, "message") <- "500 - Server-side error. Please contact your histtext server administrator."
    return(has_error)
  }
  if (response$status_code != 200) {
    has_error <- TRUE
    json <- httr::content(response)
    message <- if (is.list(json) && !is.null(json$detail)) {
      json$detail
    } else {
      "Please report error to your histtext server administrator."
    }
    attr(has_error, "message") <- paste(response$status_code, "-", message)
    return(has_error)
  }
  content_type <- httr::headers(response)$`content-type`
  if (content_type == "application/json") {
    json <- httr::content(response)
    if (is.list(json) && !is.null(json$status) && json$status != "OK") {
      has_error <- TRUE
      if (!is.null(json$message))
        attr(has_error, "message") <- json$message
      else
        attr(has_error, "message") <- json$status
      return(has_error)
    }
  }

  FALSE
}



#' GET a resource from the server
#'
#' @param resource a string representing the path to the resource to GET on the
#'   server
#' @param query a named list of queries
#' @return a `httr::response()` object
#' @export
query_server_get <- function(resource, query = list(), verbose = FALSE) {
  server_info <- get_server_info()

  url <- stringr::str_c(server_info$domain,
                        resource,
                        sep = "")
  if (verbose) {
    print(paste(url))
    print(query)
  }

  r <- tryCatch({
    if ("user" %in% names(server_info)) {
      httr::with_config(
              httr::authenticate(
                      server_info$user,
                      server_info$password,
                      "basic"
                    ),
              httr::GET(url, query = query)
            )
    } else {
      httr::GET(url, query = query)
    }
  }, error = function(e) {
    message(e)
    NULL
  })

  has_error <- get_error_status(r)
  if (has_error) {
    stop(attr(has_error, "message"))
  }
  r
}


#' POST a file to the server
#'
#' @param resource a string representing the path to the resource to GET on the
#'   server
#' @param config a httr config required by the POST request
#' @param body a string body to POST to the server
#' @param query a named list of queries
#' @return a `httr::response()` object
#' @export
query_server_post <- function(resource, config, query = list(), body = "", verbose = FALSE) {
  server_info <- get_server_info()

  url <- stringr::str_c(server_info$domain,
                        resource,
                        sep = "")
  if (verbose) {
    print(url)
  }
  r <- tryCatch({
    if ("user" %in% names(server_info)) {
      httr::with_config(
              httr::authenticate(
                      server_info$user,
                      server_info$password,
                      "basic"
                    ),
              httr::POST(url, config, query = query, body = body)
            )
    } else {
      httr::POST(url, config, query = query, body = body)
    }
  }, error = function(e) {
    NULL
  })

  has_error <- get_error_status(r)
  if (has_error) {
    stop(attr(has_error, "message"))
  }
  r
}


#' Get the status of the server
#'
#' @return the status as a string
#' @export
get_server_status <- function() {
  response <- query_server_get("/")
  json_str <- httr::content(response, as = "text", encoding = "utf-8")
  json <- jsonlite::fromJSON(json_str)
  json$status
}
