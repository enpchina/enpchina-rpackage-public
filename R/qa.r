
#' List available NER models on the server
#'
#' @return a vector of strings listing the models
#' @export
list_qa_models <- function(verbose = FALSE) {
  x <- query_server_get("/qa/list_models", verbose = verbose)
  json <- httr::content(x, as = "text", encoding = "utf-8")
  jsonlite::fromJSON(json)
}


#' Convert a biography category to the corresponding QA questions.
#'
#' @param language language of the questions (only support 'en' and 'zh')
#' @return a named list of question vectors
#' @export
biography_questions <- function(language) {
  if (language == "en") {
    list(
      "name:full" = c("What name?", "What is his full name?"),
      "birth:location" = c(
        "Where born?", "In what location is he born?",
        "In what location is {name:full} born?"
      ),
      "birth:year" = c(
        "When born?", "What year is he born?",
        "What year is {name:full} born?"
      ),
      "education:location" = c(
        "Where {name:full} study at?", "Where study at?",
        "What school, college or university did {name:full} attend?"
      ),
      "education:year" = c(
        "When {name:full} study at {#education:location}?",
        "When study at {#education:location}?"
      ),
      "position:job" = c("What job position?", "What job?"),
      "position:job_location" = c(
        "Where {name:full} was {#position:job}?",
        "In what location {#position:job}?",
        "Where {#position:job}?"
      ),
      "position:job_year" = c(
        "When {name:full} was {#position:job}?",
        "What year {#position:job}?",
        "When {#position:job}?"
      )
    )
  } else if (language == "zh") {
    list(
      "name:full" = c("什麼名字？"),
      "name:given" = c("名什麼？"),
      "name:art" = c("號是什麼？"),
      "name:courtesy" = c("字是什麼？"),
      "birth:location" = c(
        "哪裡出生？",
        "他從哪裡來？"
      ),
      "birth:age" = c("他幾歲了？"),
      "education:location" = c(
        "哪裡{%zh}上學？",
        "上過什麼{%zh}學校或大學？",
        "什麼{%zh}教育？"
      ),
      "education:level" = c("您的學歷是多少？"),
      "position:job" = c("什麼工作？")
    )
  } else {
    NULL
  }
}


#' Apply Question-Answering on a string
#'
#' @param text to be analyzed
#' @param questions a vector of questions or a named list of question vectors
#'   (see biography_questions() for an example)
#' @param model to be used (use `list_qa_models()` to get available models)
#' @param max_answers an integer or a named list with keys corresponding to keys
#'   in `questions`. If not a named list, value will be used for all questions.
#' @param threshold a float, NULL or a named list with keys corresponding to
#'   keys in `questions`. If not a named list, value will be used for all
#'   questions.
#' @export
run_qa <- function(text, questions,
                   model = "trftc_biography:en:qa",
                   max_answers = 1,
                   threshold = NULL,
                   verbose = FALSE) {
  url <- stringr::str_c("/qa/", model, "/extract")
  query <- list(
    model = model,
    field = field,
  )
  body <- list(
    questions = questions,
    text = text,
    max_answers = max_answers,
    threshold = threshold
  )
  resp <- query_server_post(
    url,
    httr::add_headers(`Content-Type` = "text/plain"),
    body = body,
    query = query,
    verbose = FALSE
  )
  result <- readr::read_csv(httr::content(resp, "text", encoding = "utf-8"),
    col_types = readr::cols(
      .default = readr::col_character(),
      Start = readr::col_integer(),
      End = readr::col_integer(),
      Score = readr::col_double()
    )
  )
  unique(result)
}

#' Apply Named Entity Recognition on the specified column of a dataframe
#'
#' @param df Dataframe which contains texts to analyze
#' @param questions a vector of questions or a named list of question vectors
#'   (see biography_questions() for an example)
#' @param text_column to use in the dataframe which contains the input text.
#' @param id_column that is used to associate ids to the QA outputs. By
#'   default, uses the row index in `df`.
#' @param model to be used (use `list_qa_models()` to get available models)
#' @param max_answers an integer or a named list with keys corresponding to keys
#'   in `questions`. If not a named list, value will be used for all questions.
#' @param threshold a float, NULL or a named list with keys corresponding to
#'   keys in `questions`. If not a named list, value will be used for all
#'   questions.
#' @export
qa_on_df <- function(df, questions, text_column,
                     id_column = NULL,
                     model = "trftc_biography:en:qa",
                     max_answers = 1,
                     threshold = NULL,
                     verbose = TRUE) {
  result <- NULL
  for (i in 1:nrow(df)) {
    if (verbose && i %% 10 == 1) {
      print(stringr::str_glue("{i}/{nrow(df)}"))
    }
    qa_output <- run_qa(df[i, text_column], questions,
                        model = model,
                        max_answers = max_answers,
                        threshold = threshold,
                        verbose = FALSE)
    id <- if (is.null(id_column)) i else df[i, id_column]
    qa_output[, if (is.null(id_column)) "TextIndex" else id_column] <- id

    if (is.null(result) || dim(result)[1] == 0) result <- qa_output
    else if (dim(qa_output)[1] > 0)  result <- dplyr::bind_rows(result,
                                                                 qa_output)
  }
  result
}

#' Apply Named Entity Recognition on a corpus
#'
#' @param docIDs Dataframe of documents which includes an `DocId` column
#' @param questions a vector of questions or a named list of question vectors
#'   (see biography_questions() for an example)
#' @param corpus to be used
#' @param model to be used (the default model for the corpus is used if not
#'   specified)
#' @param field on which will be applied the model
#' @param max_answers an integer or a named list with keys corresponding to keys
#'   in `questions`. If not a named list, value will be used for all questions.
#' @param threshold a float, NULL or a named list with keys corresponding to
#'   keys in `questions`. If not a named list, value will be used for all
#'   questions.
#' @export
qa_on_corpus <- function(docIDs, questions, corpus="proquest",
                         model = "trftc_biography:en:qa",
                         field = "__default__",
                         max_answers = 1,
                         threshold = NULL,
                         batch_size = 1,
                         verbose = TRUE) {
  url <- stringr::str_c("/", corpus,  "-qa/extract",
                        sep = "")
  query <- list(model = model,
                field = field
                )
  ids <- if ("DocId" %in% colnames(docIDs)) docIDs$DocId else docIDs$Id
  nb_ids <- length(ids) - 1
  step <- batch_size
  result <- NULL
  for (start in 0:(floor(nb_ids / step))) {
    start <- start * step + 1
    end <- start + step - 1
    if (verbose)
      print(stringr::str_glue("{start}/{nb_ids + 1}"))
    if (length(na.omit(ids[start:end])) == 0) {
      warning("Encountered only NA ids in qa_on_corpus - please inform package maintainer")
      next
    }
    body <- list(
      questions = questions,
      docids = I(na.omit(ids[start:end])),
      max_answers = max_answers,
      threshold = threshold
    )
    resp <- query_server_post(
      url,
      httr::add_headers(`Content-Type` = "text/plain"),
      body = jsonlite::toJSON(body, auto_unbox = TRUE),
      query = query,
      verbose = FALSE
    )
    batch <- readr::read_csv(
      httr::content(resp, "text", encoding = "utf-8"),
      col_types = readr::cols(
        .default = readr::col_character(),
        Start = readr::col_integer(),
        End = readr::col_integer(),
        Score = readr::col_double()
      )
    )
    if (is.null(result) || dim(result)[1] == 0) result <- batch
    else if (dim(batch)[1] > 0) result <- dplyr::bind_rows(result, batch)
  }
  result
}
