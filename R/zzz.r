
.onLoad <- function(libname, pkgname) {
  assign("enp_config_path",
         system.file("enpconfig.yml", package = "histtext"),
         envir = topenv())

  config <- yaml::yaml.load_file(enp_config_path)
  if (is.null(config) || "NOTSET" %in% names(config)) {
    warning("Server configuration is not set. Please use the histtext::set_config_file function.")
  }
}
