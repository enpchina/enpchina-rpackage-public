
#' List available NER models on the server
#'
#' @return a vector of strings listing the models
#' @export
list_ner_models <- function(verbose = FALSE) {
  x <- query_server_get("/ner/list_models", verbose = verbose)
  json <- httr::content(x, as = "text", encoding = "utf-8")
  jsonlite::fromJSON(json)
}


#' List corpora with precomputed annotations
#'
#' @param task to check for precomputed models (currently only supports ner)
#' @return a vector of strings listing the precomputed corpora
#' @export
list_precomputed_corpora <- function(task = "ner", verbose = FALSE) {
  x <- query_server_get(paste0("/", task, "/list_precomputed_collections"),
                        verbose = verbose)
  json <- httr::content(x, as = "text", encoding = "utf-8")
  jsonlite::fromJSON(json)
}


#' List fields of a given corpus that have precomputed annotations
#'
#' @param corpus to get the information on
#' @param task to check for precomputed models (currently only supports ner)
#' @return a vector of strings listing the precomputed fields
#' @export
list_precomputed_fields <- function(corpus, task = "ner", verbose = FALSE) {
  x <- query_server_get(paste0("/",
                               corpus, "-", task,
                               "/list_precomputed_fields"),
      verbose = verbose
    )
    json <- httr::content(x, as = "text", encoding = "utf-8")
    jsonlite::fromJSON(json)
}

#' Get the name of the default NER model for a given corpus
#'
#' @param corpus to get the information on
#' @return the name of the default NER model
#' @export
get_default_ner_model <- function(corpus, verbose = FALSE) {
  x <- query_server_get(paste0("/ner/get_default_model/", corpus), verbose)
  json <- httr::content(x, as = "text", encoding = "utf-8")
  jsonlite::fromJSON(json)$model
}


#' Apply Named Entity Recognition on a string
#'
#' @param text to be analyzed
#' @param model to be used (use `list_ner_models()` to get available models)
#' @note If given text is longer than 2048 characters, it will be split into
#'   smaller chunks. Preprocess texts beforehand if you need clean splits.
#' @note Currently, long text results are not perfectly merged back together.
#' @export
run_ner <- function(text, model="spacy:en:ner", verbose = FALSE) {
  url <- stringr::str_c("/ner/", model, "/extract")
  maxlen <- 2048
  if (nchar(text) > maxlen) {
    warning("Text is longer than 2048 characters, applied a quick and dirty split.")
  }
  stride <- 16
  text_splits <- split_text_for_ner(text, maxlen = maxlen,
                                    stride = stride)
  result <- NULL
  for (split_idx in seq_along(text_splits$subtexts)) {
    subtext <- text_splits$subtexts[split_idx]
    offset <- text_splits$offsets[split_idx]
    resp <- query_server_post(
      url,
      httr::add_headers(`Content-Type` = "text/plain"),
      body = subtext,
      verbose = FALSE
    )
    batch <- readr::read_csv(httr::content(resp, "text", encoding = "utf-8"),
      col_types = readr::cols(
        .default = readr::col_character(),
        Start = readr::col_integer(),
        End = readr::col_integer(),
        Confidence = readr::col_double()
      )
    )
    if (dim(batch)[1] > 0) {
      batch$Start <- batch$Start + offset
      batch$End <- batch$End + offset
    }
    if (is.null(result) || dim(result)[1] == 0) result <- batch
    else if (dim(batch)[1] > 0) result <- dplyr::bind_rows(result, batch)
  }
  # TODO: Merge back NER result to remove duplicates because of stride
  unique(result)
}

#' Apply Named Entity Recognition on the specified column of a dataframe
#'
#' @param df Dataframe which contains texts to analyze
#' @param text_column to use in the dataframe which contains the input text.
#' @param id_column that is used to associate ids to the NER outputs. By
#'   default, uses the row index in `df`.
#' @param model to be used (use `list_ner_models()` to get available models)
#' @note If a given text is longer than 2048 characters, it will be split into
#'   smaller chunks. Preprocess texts beforehand if you need clean splits.
#' @export
ner_on_df <- function(df, text_column, id_column = NULL,
                      model = "spacy:en:ner",
                      verbose = TRUE) {
  result <- NULL
  for (i in 1:nrow(df)) {
    if (verbose && i %% 10 == 1) {
      print(stringr::str_glue("{i}/{nrow(df)}"))
    }
    ner_output <- run_ner(toString(df[i, text_column]), model = model,
                          verbose = FALSE)
    id <- if (is.null(id_column)) i else df[i, id_column]
    ner_output[, if (is.null(id_column)) "TextIndex" else id_column] <- id

    if (is.null(result) || dim(result)[1] == 0) result <- ner_output
    else if (dim(ner_output)[1] > 0)  result <- dplyr::bind_rows(result,
                                                                 ner_output)
  }
  result
}

#' Apply Named Entity Recognition on a corpus
#'
#' @param docIDs Dataframe of documents which includes an `DocId` column
#' @param corpus to be used
#' @param model to be used (the default model for the corpus is used if not
#'   specified)
#' @param field on which will be applied the model
#' @param only_precomputed if TRUE, only returns precomputed NER predictions
#' @export
ner_on_corpus <- function(docIDs, corpus="proquest",
                          model = "__default__",
                          field = "__default__",
                          only_precomputed = TRUE,
                          batch_size = 10,
                          verbose = TRUE) {
  url <- stringr::str_c("/", corpus,  "-ner/extract",
                        sep = "")
  query <- list(model = model,
                field = field,
                precomputed = if (only_precomputed) "true" else "false")

  ids <- if ("DocId" %in% colnames(docIDs)) docIDs$DocId else docIDs$Id
  nb_ids <- length(ids) - 1
  step <- batch_size
  result <- NULL
  for (start in 0:(floor(nb_ids / step))) {
    start <- start * step + 1
    end <- start + step - 1
    if (verbose)
      print(stringr::str_glue("{start}/{nb_ids + 1}"))
    if (length(na.omit(ids[start:end])) == 0) {
      warning("Encountered only NA ids in ner_on_corpus - please inform package maintainer")
      next
    }
    body <- stringr::str_c(na.omit(ids[start:end]), collapse = "\n")
    resp <- query_server_post(
      url,
      httr::add_headers(`Content-Type` = "text/plain"),
      body = body,
      query = query,
      verbose = FALSE
    )
    batch <- readr::read_csv(
      httr::content(resp, "text", encoding = "utf-8"),
      col_types = readr::cols(
        .default = readr::col_character(),
        Start = readr::col_integer(),
        End = readr::col_integer(),
        Confidence = readr::col_double()
      )
    )
    if (is.null(result) || dim(result)[1] == 0) result <- batch
    else if (dim(batch)[1] > 0) result <- dplyr::bind_rows(result, batch)
  }
  result
}


split_text_for_ner <- function(text, maxlen = 256,
                               stride = 16) {
  to_split <- c(text)
  to_split_offsets <- c(0)
  result <- c()
  offsets <- c()

  while (length(to_split) != 0) {
    element <- to_split[1]
    offset <- to_split_offsets[1]
    to_split <- tail(to_split, -1)
    to_split_offsets <- tail(to_split_offsets, -1)

    if (nchar(element) <= maxlen) {
      result <- append(result, element)
      offsets <- append(offsets, offset)
      next
    }

    splits <- split_text(element, maxlen, stride)
    result <- append(result, splits$subtexts)
    offsets <- append(offsets, splits$offsets + offset)
  }

  list("subtexts" = result, "offsets" = offsets)
}


split_text <- function(text, maxlen, stride) {
  text_splits <- c()
  offsets <- c()
  past_context <- ""
  true_maxlen <- maxlen - 2 * stride
  current_offset <- 0
  while (nchar(text) != 0) {
    current_subtext <- substring(text, 1, true_maxlen)
    future_context <- substring(text, true_maxlen + 1, true_maxlen + stride)

    window <- paste(past_context, current_subtext, future_context, sep = "")
    text_splits <- append(text_splits, window)
    offsets <- append(offsets, current_offset)
    current_offset <- current_offset + true_maxlen - stride
    past_context <- substring(text, true_maxlen - stride + 1, true_maxlen)
    text <- substring(text, true_maxlen + 1)
  }
  list("subtexts" = text_splits, "offsets" = offsets)
}
