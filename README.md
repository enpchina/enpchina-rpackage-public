The HistText library is available here for those who would be interested in
implementing the same set of functions and to apply them to their own corpora.
This requires skills to connect the library to the relevant corpora on a given
server. Yet, this can be done easily with the help of a computer scientist.

## Purpose of the HistText package

This package allows anyone with some minimal skills in R to query a SolR
database. It also includes natural language processing functions such as Named
Entity Recognition, which can be easily applied on queried documents.

**Important notice:** This package is only a client. It relies on a server
instance of the HistText Server API
(https://gitlab.com/enpchina/histtext-server-api). If you are granted access to
an already existing server instance, you can simply setup the R client as
instructed in the following section. Otherwise, you'll have to setup the server
(a SolR server will also be required) in order to be able to use this package.

## Installation

The package is not (yet) available on CRAN. The installation must be done from
this repository. The easiest way is to use the `devtools` package:

```
devtools::install_gitlab("enpchina/histtext-r-client")
```

Before the first use of the package, you must call the `set_config_file` function:
```
histtext::set_config_file(path = "enpconfig.yml")
```

The YAML file must be as follows:
```
server:
  user: "myuser"  # ONLY REQUIRED IF REQUIRED BY SERVER
  password: "mypassword"  # ONLY REQUIRED IF REQUIRED BY SERVER
  domain: "https://my.server.domain.com"
```

Instead of providing a YAML file, you can also directly set the required
information as arguments of the function:
```
histtext::set_config_file(domain = "https://my.server.domain.com", 
                          user = "myuser", password = "mypassword")
```

## Updates and basic tutorials

General Manual: https://bookdown.enpchina.eu/rpackage/HistTextRManual.html

Update notes:
- Update 1.0.0: https://bookdown.enpchina.eu/rpackage/1.0.0.html
- Update 0.2.5: https://bookdown.enpchina.eu/rpackage/0.2.5.html
